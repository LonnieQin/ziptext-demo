//
//  ZipTextView.h
//  ZipTestDemo
//
//  Created by amttgroup on 15-5-25.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import <UIKit/UIKit.h>
#define REVISION 5
@interface ZipTextView : UIView
- (instancetype) initWithFrame:(CGRect)frame text:(NSString*) text;
@end
