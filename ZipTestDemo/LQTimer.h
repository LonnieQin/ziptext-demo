//
//  LQTimer.h
//  ZipTestDemo
//
//  Created by amttgroup on 15-5-25.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LQTimer : NSObject
+ (instancetype) repeatingTimerWithTimeInterval:(NSTimeInterval) seconds block:(void(^)(void)) block;
- (void) fire;
- (void) invalidate;
@end
