//
//  ZipTextView5.m
//  ZipTestDemo
//
//  Created by amttgroup on 15-5-26.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "ZipTextView.h"
#import "LQTimer.h"
#if REVISION == 5
static const CGFloat kFontSize = 17.0;
@interface ZipTextView()
@property (nonatomic) BOOL shouldRotate;
@property (nonatomic) NSUInteger index;
@property (nonatomic) LQTimer * timer;
@property (nonatomic) NSString * text;
@property (nonatomic) NSCache * origins;
@end
@implementation ZipTextView
- (instancetype) initWithFrame:(CGRect)frame text:(NSString *)text
{
    self = [super initWithFrame:frame];
    if (self) {
        __weak ZipTextView * weakSelf = self;
        _timer = [LQTimer repeatingTimerWithTimeInterval:0.01 block:^{
            [weakSelf appendNextCharacter];
        }];
        _text = [text copy];
        self.backgroundColor = [UIColor whiteColor];
        _origins = [NSCache new];
        [_origins setObject:[NSValue valueWithCGPoint:CGPointZero] forKey:@0];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(rotate:) name:UIDeviceOrientationDidChangeNotification object:nil];
    }
    return self;
}

- (void) appendNextCharacter
{
    if (++self.index < self.text.length) {
        CGRect dirtyRect;
        dirtyRect.origin = [self originAtIndex:self.index fontSize:kFontSize];
        dirtyRect.size = CGSizeMake(kFontSize,kFontSize);
        [self setNeedsDisplayInRect:dirtyRect];
    }
}

- (void) rotate:(NSNotification*) n
{
    NSLog(@"%s",__func__);
    self.shouldRotate = YES;
    if (_origins != nil) {
        [_origins removeAllObjects];
        [_origins setObject:[NSValue valueWithCGPoint:CGPointZero] forKey:@0];
    }
    [self setNeedsDisplay];
}

- (void) drawRect:(CGRect)rect
{
    if (self.shouldRotate) {
         self.shouldRotate = NO;
        for (NSUInteger i = 0; i < self.index; ++i) {
            if (i < self.text.length) {
                NSString * character = [self.text substringWithRange:NSMakeRange(i, 1)];
                CGPoint origin = [self originAtIndex:i fontSize:kFontSize];
                [character drawAtPoint:origin withAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:kFontSize]}];
            }
        }
       
    } else {
        NSString * character = [self.text substringWithRange:NSMakeRange(self.index, 1)];
        CGPoint origin = [self originAtIndex:self.index fontSize:kFontSize];
        if (CGRectContainsPoint(rect,origin)) {
            [character drawAtPoint:origin withAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:kFontSize]}];
        }
    }
}


- (CGPoint) originAtIndex:(NSUInteger) index fontSize:(CGFloat) fontSize
{
    if ([self.origins objectForKey:@(index)] != nil) {
        return [[self.origins objectForKey:@(index)] CGPointValue];
    } else {
        CGPoint origin = [self originAtIndex:index-1 fontSize:fontSize];
        NSString * pregvCharacter = [self.text substringWithRange:NSMakeRange(index-1, 1)];
        CGSize prevCharacterSize = [pregvCharacter sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize]}];
        origin.x += prevCharacterSize.width;
        if (origin.x > CGRectGetWidth(self.bounds)) {
            origin.x = 0;
            origin.y += prevCharacterSize.height;
        }
        [self.origins setObject:[NSValue valueWithCGPoint:origin] forKey:@(index)];
        return origin;
    }
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
#endif