//
//  ZipTextView2.m
//  ZipTestDemo
//
//  Created by amttgroup on 15-5-26.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//
#import "ZipTextView.h"
#import "LQTimer.h"
#if REVISION == 2
@interface ZipTextView()
@property (nonatomic) NSUInteger index;
@property (nonatomic) LQTimer * timer;
@property (nonatomic) NSString * text;
@end
@implementation ZipTextView
- (instancetype) initWithFrame:(CGRect)frame text:(NSString *)text
{
    NSLog(@"%s",__func__);
    self = [super initWithFrame:frame];
    if (self) {
        __weak ZipTextView * weakSelf = self;
        _timer = [LQTimer repeatingTimerWithTimeInterval:0.01 block:^{
            [weakSelf appendNextCharacter];
        }];
        _text = [text copy];
        
    }
    return self;
}

- (void) appendNextCharacter
{
    NSUInteger i = self.index;
    if (i < self.text.length) {
        UILabel * label = [[UILabel alloc] init];
        label.text = [self.text substringWithRange:NSMakeRange(i, 1)];
        label.opaque = NO;
        [label sizeToFit];
        CGRect frame = label.frame;
        frame.origin = [self originAtIndex:i fontSize:label.font.pointSize];
        label.frame = frame;
        [self addSubview:label];
    }
    ++self.index;
}

- (CGPoint) originAtIndex:(NSUInteger) index fontSize:(CGFloat) fontSize
{
    if (index == 0) {
        return CGPointZero;
    } else {
        CGPoint origin = [self originAtIndex:index-1 fontSize:fontSize];
        NSString * pregvCharacter = [self.text substringWithRange:NSMakeRange(index-1, 1)];
        CGSize prevCharacterSize = [pregvCharacter sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize]}];
        origin.x += prevCharacterSize.width;
        if (origin.x > CGRectGetWidth(self.bounds)) {
            origin.x = 0;
            origin.y += prevCharacterSize.height;
        }
        return origin;
    }
}

@end
#endif