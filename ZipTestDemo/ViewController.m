//
//  ViewController.m
//  ZipTestDemo
//
//  Created by amttgroup on 15-5-25.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "ViewController.h"
#import "ZipTextView.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSString * path = [[NSBundle mainBundle] pathForResource:@"Lorem" ofType:@"txt"];
    ZipTextView * ztView = [[ZipTextView alloc] initWithFrame:self.view.bounds text:[NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil]];
    ztView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:ztView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) prefersStatusBarHidden {return YES;}

@end
